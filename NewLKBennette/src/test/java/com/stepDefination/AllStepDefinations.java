package com.stepDefination;

import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;


public class AllStepDefinations {
	
	public static WebDriver driver;
	public static  WebDriverWait wait;
	
	@Given("^I am in homepage\\.$")
	public void i_am_in_homepage() throws Throwable {
		System.setProperty("webdriver.chrome.driver","C:/Users/sures/OneDrive/Desktop/Automation/chromedriver.exe");
        driver=new ChromeDriver();
		driver.get("https://www.lkbennett.com/");
	    driver.manage().window().maximize();
	    	}

	@When("^I enter valid name$")
	public void i_enter_valid_name() throws Throwable {
		 driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
	     driver.findElements(By.cssSelector(".input-group .ui-autocomplete-input")).get(0).clear();
	     driver.findElements(By.cssSelector(".input-group .ui-autocomplete-input")).get(0).sendKeys("boots");
		 driver.findElements(By.cssSelector(".input-group-btn .btn-link .icon--primary")).get(0).click();
		
	 	}

	@Then("^I should be able to see the search results page$")
	public void i_should_be_able_to_see_the_search_results_page() throws Throwable {
		
		Assert.assertEquals("BOOTS", driver.findElement(By.cssSelector("h1")).getText());
 }
	
	@When("^I enter invalid name$")
	public void i_enter_invalid_name() throws Throwable {
		 driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
	     driver.findElements(By.cssSelector(".input-group .ui-autocomplete-input")).get(0).clear();
	     driver.findElements(By.cssSelector(".input-group .ui-autocomplete-input")).get(0).sendKeys("hgahagdh");
		 driver.findElements(By.cssSelector(".input-group-btn .btn-link .icon--primary")).get(0).click();
		  
	}

	@Then("^I should be able to see the invalid search results page\\.$")
	public void i_should_be_able_to_see_the_invalid_search_results_page() throws Throwable {
		 Assert.assertEquals("https://www.lkbennett.com/search/?text=hgahagdh", driver.getCurrentUrl());
	 
	}
	@When("^I enter empty space\\.$")
	public void i_enter_empty_space() throws Throwable {
		 driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
	     driver.findElements(By.cssSelector(".input-group .ui-autocomplete-input")).get(0).clear();
	     driver.findElements(By.cssSelector(".input-group .ui-autocomplete-input")).get(0).sendKeys(" ");
		 driver.findElements(By.cssSelector(".input-group-btn .btn-link .icon--primary")).get(0).click();
	    
	}

	@Then("^I should be able to see the search results page for empty data$")
	public void i_should_be_able_to_see_the_search_results_page_for_empty_data() throws Throwable {
		
		 Assert.assertEquals("Search | LKBennett-UK", driver.getTitle()); 
	     Assert.assertEquals("https://www.lkbennett.com/search/?text=+", driver.getCurrentUrl());

	   	}
	@Given("^I am in signin page$")
	public void i_am_in_signin_page() throws Throwable {
		System.setProperty("webdriver.chrome.driver","C:/Users/sures/OneDrive/Desktop/Automation/chromedriver.exe");
        driver=new ChromeDriver();
		driver.get("https://www.lkbennett.com/");
	    driver.manage().window().maximize();
	    WebDriverWait wait=new WebDriverWait(driver,10);
		Actions action=new Actions(driver);
		WebElement element=driver.findElement(By.cssSelector("#jsSignInPopup"));
		action.moveToElement(element).perform();
		driver.findElement(By.cssSelector(".info-container .jsSignInbtn.ts-2x")).click();
	
	}
	    
	@When("^I enter valid details$")
	public void i_enter_valid_details() throws Throwable {
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.findElement(By.cssSelector("#j_username")).clear();
		driver.findElement(By.cssSelector("#j_username")).sendKeys("vanam05work@gmail.com");
		driver.findElement(By.cssSelector("#j_password")).clear();
		driver.findElement(By.cssSelector("#j_password")).sendKeys("Padmavathi100");
		driver.findElements(By.cssSelector("#loginForm .btn-block.btn--large")).get(0).click();

	  	}

	@Then("^I should see the welcome page$")
	public void i_should_see_the_welcome_page() throws Throwable {
	   

     	Assert.assertEquals("https://www.lkbennett.com/", driver.getCurrentUrl());
	    Assert.assertEquals("L.K.Bennett | Luxury Women�s Clothing & Fashion", driver.getTitle());

	}
	
	@When("^I enter invalid id and valid pwd$")
	public void i_enter_invalid_id_and_valid_pwd() throws Throwable {
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.findElement(By.cssSelector("#j_username")).clear();
		driver.findElement(By.cssSelector("#j_username")).sendKeys("vanam05work@");
		driver.findElement(By.cssSelector("#j_password")).clear();
		driver.findElement(By.cssSelector("#j_password")).sendKeys("Padmavathi100");
		driver.findElements(By.cssSelector("#loginForm .btn-block.btn--large")).get(0).click();
	}

	@Then("^I should see the error message$")
	public void i_should_see_the_error_message() throws Throwable {
		System.out.println("assert should be written");
	  
	}

	@When("^I enter the valid id and invalid pwd$")
	public void i_enter_the_valid_id_and_invalid_pwd() throws Throwable {
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.findElement(By.cssSelector("#j_username")).clear();
		driver.findElement(By.cssSelector("#j_username")).sendKeys("vanam05work@gmail.com");
		driver.findElement(By.cssSelector("#j_password")).clear();
		driver.findElement(By.cssSelector("#j_password")).sendKeys("$%^&*");
		driver.findElements(By.cssSelector("#loginForm .btn-block.btn--large")).get(0).click();
	  	}

	@Then("^I should see the errorinvalid pwd message$")
	public void i_should_see_the_errorinvalid_pwd_message() throws Throwable {
		Assert.assertEquals("https://www.lkbennett.com/login?error=true", driver.getCurrentUrl());
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		Assert.assertEquals("Your username or password was incorrect.", driver.findElement(By.cssSelector(".alert.alert-danger")).getText());
		
	}


	@When("^I enter empty data$")
	public void i_enter_empty_data() throws Throwable {
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.findElement(By.cssSelector("#j_username")).clear();
		driver.findElement(By.cssSelector("#j_username")).sendKeys(" ");
		driver.findElement(By.cssSelector("#j_password")).clear();
		driver.findElement(By.cssSelector("#j_password")).sendKeys(" ");
		driver.findElements(By.cssSelector("#loginForm .btn-block.btn--large")).get(0).click();

	}

	@Then("^I should see the error messa$")
	public void i_should_see_the_error_messa() throws Throwable {
		Assert.assertEquals("https://www.lkbennett.com/login?error=true", driver.getCurrentUrl());
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		Assert.assertEquals("Your username or password was incorrect.", driver.findElement(By.cssSelector(".alert.alert-danger")).getText());
		
		}

	@When("^I enter the validreg details$")
	public void i_enter_the_validreg_details() throws Throwable {
		    driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		    Select title=new Select(driver.findElement(By.id("register.title")));
	        title.selectByVisibleText("Mr");
			title.selectByIndex(1);		
			driver.findElement(By.id("register.firstName")).clear();
			driver.findElement(By.id("register.firstName")).sendKeys("mala");
			driver.findElement(By.id("register.lastname")).clear();		
			driver.findElement(By.id("register.lastname")).sendKeys("acha");		
			driver.findElement(By.id("register.email")).clear();		
			driver.findElement(By.id("register.email")).sendKeys("vanam08work@gmail.com");		
			driver.findElement(By.id("register.confirmemail")).clear();
			driver.findElement(By.id("register.confirmemail")).sendKeys("vanam08work@gmail.com");		
			driver.findElement(By.id("register.password")).sendKeys("Padmavathi100");		
			driver.findElement(By.id("register.checkPwd")).sendKeys("Padmavathi100");			
			driver.findElement(By.cssSelector("#createAccountButton")).click();
		
	   	}

	@Then("^I should see the successfully created account page$")
	public void i_should_see_the_successfully_created_account_page() throws Throwable {
		Assert.assertEquals("https://www.lkbennett.com/my-account/profile", driver.getCurrentUrl());
		Assert.assertEquals("Thank you for registering.", driver.findElement(By.cssSelector(".alert.alert-info")).getText());
		Assert.assertEquals("Profile | L.K.Bennett", driver.getTitle());
	   	}
	


	@When("^I enter invalid email$")
	public void i_enter_invalid_email() throws Throwable {
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		Select title=new Select(driver.findElement(By.id("register.title")));
        title.selectByVisibleText("Mr");
		title.selectByIndex(1);		
		driver.findElement(By.id("register.firstName")).clear();
		driver.findElement(By.id("register.firstName")).sendKeys("mala");
		driver.findElement(By.id("register.lastname")).clear();		
		driver.findElement(By.id("register.lastname")).sendKeys("acha");		
		driver.findElement(By.id("register.email")).clear();		
		driver.findElement(By.id("register.email")).sendKeys("vanam05work");	
		driver.findElement(By.id("register.confirmemail")).clear();
		driver.findElement(By.id("register.confirmemail")).sendKeys("vanam05work@gmail.com");
		driver.findElement(By.id("register.password")).sendKeys("Padmavathi100");		
		driver.findElement(By.id("register.checkPwd")).sendKeys("Padmavathi100");
		//driver.findElement(By.cssSelector("#createAccountButton")).click();
	
	   }

	@Then("^I should see the email-error message$")
	public void i_should_see_the_email_error_message() throws Throwable {
		 Assert.assertEquals("https://www.lkbennett.com/login", driver.getCurrentUrl());
		    driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			//Assert.assertEquals("Please enter a valid email", driver.findElement(By.cssSelector(".form-group .error-msg.js-errorMsg")).getText());
		
	 	}

	@When("^I enter invalidconfirm mail$")
	public void i_enter_invalidconfirm_mail() throws Throwable {
	        driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
		    Select title=new Select(driver.findElement(By.id("register.title")));
	        title.selectByVisibleText("Mr");
			title.selectByIndex(1);		
			driver.findElement(By.id("register.firstName")).clear();
			driver.findElement(By.id("register.firstName")).sendKeys("mala");
			driver.findElement(By.id("register.lastname")).clear();		
			driver.findElement(By.id("register.lastname")).sendKeys("acha");		
			driver.findElement(By.id("register.email")).clear();		
			driver.findElement(By.id("register.email")).sendKeys("vanam05work@gmail.com");	
			driver.findElement(By.id("register.confirmemail")).clear();
			driver.findElement(By.id("register.confirmemail")).sendKeys("vanam05work@");
			driver.findElement(By.id("register.password")).sendKeys("Padmavathi100");		
			driver.findElement(By.id("register.checkPwd")).sendKeys("Padmavathi100");
			//driver.findElement(By.cssSelector("#createAccountButton")).click();

	}

	@Then("^I should see the confirmmail-error message$")
	public void i_should_see_the_confirmmail_error_message() throws Throwable {
		    Assert.assertEquals("https://www.lkbennett.com/login", driver.getCurrentUrl());
		   }
	
	@When("^I enter invalid pwd$")
	public void i_enter_invalid_pwd() throws Throwable {
		    driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
		    Select title=new Select(driver.findElement(By.id("register.title")));
	        title.selectByVisibleText("Mr");
			title.selectByIndex(1);		
			driver.findElement(By.id("register.firstName")).clear();
			driver.findElement(By.id("register.firstName")).sendKeys("mala");
			driver.findElement(By.id("register.lastname")).clear();		
			driver.findElement(By.id("register.lastname")).sendKeys("acha");		
			driver.findElement(By.id("register.email")).clear();		
			driver.findElement(By.id("register.email")).sendKeys("vanam05work@gmail.com");	
			driver.findElement(By.id("register.confirmemail")).clear();
			driver.findElement(By.id("register.confirmemail")).sendKeys("vanam05work@gmail.com");
			driver.findElement(By.id("register.password")).sendKeys("Padma");		
			driver.findElement(By.id("register.checkPwd")).sendKeys("Padmavathi100");
			//driver.findElement(By.cssSelector("#createAccountButton")).click();

	}

	@Then("^I should see the invalidpwd-error$")
	public void i_should_see_the_invalidpwd_error() throws Throwable {
		Assert.assertEquals("https://www.lkbennett.com/login", driver.getCurrentUrl());
	  }

	@When("^I enter invalidconfirm pwd$")
	public void i_enter_invalidconfirm_pwd() throws Throwable {
		    driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
		    Select title=new Select(driver.findElement(By.id("register.title")));
	        title.selectByVisibleText("Mr");
			title.selectByIndex(1);		
			driver.findElement(By.id("register.firstName")).clear();
			driver.findElement(By.id("register.firstName")).sendKeys("mala");
			driver.findElement(By.id("register.lastname")).clear();		
			driver.findElement(By.id("register.lastname")).sendKeys("acha");		
			driver.findElement(By.id("register.email")).clear();		
			driver.findElement(By.id("register.email")).sendKeys("vanam05work@gmail.com");	
			driver.findElement(By.id("register.confirmemail")).clear();
			driver.findElement(By.id("register.confirmemail")).sendKeys("vanam05work@gmail.com");
			driver.findElement(By.id("register.password")).sendKeys("Padmavathi100");		
			driver.findElement(By.id("register.checkPwd")).sendKeys("Padma");
			//driver.findElement(By.cssSelector("#createAccountButton")).click();


	  	}

	@Then("^I should see the invalidconfirmpwd-error$")
	public void i_should_see_the_invalidconfirmpwd_error() throws Throwable {
		
		Assert.assertEquals("https://www.lkbennett.com/login", driver.getCurrentUrl());  
	}

	@When("^I enter empty spaces$")
	public void i_enter_empty_spaces() throws Throwable {
		driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
	    Select title=new Select(driver.findElement(By.id("register.title")));
        title.selectByVisibleText("Mr");
		title.selectByIndex(1);		
		driver.findElement(By.id("register.firstName")).clear();
		driver.findElement(By.id("register.firstName")).sendKeys("");
		driver.findElement(By.id("register.lastname")).clear();		
		driver.findElement(By.id("register.lastname")).sendKeys("");		
		driver.findElement(By.id("register.email")).clear();		
		driver.findElement(By.id("register.email")).sendKeys("");	
		driver.findElement(By.id("register.confirmemail")).clear();
		driver.findElement(By.id("register.confirmemail")).sendKeys("");
		driver.findElement(By.id("register.password")).sendKeys("");		
		driver.findElement(By.id("register.checkPwd")).sendKeys("");
		//driver.findElement(By.cssSelector("#createAccountButton")).click();

	    
	}

	@Then("^I should see the error messages$")
	public void i_should_see_the_error_messages() throws Throwable {
	  
		Assert.assertEquals("https://www.lkbennett.com/login", driver.getCurrentUrl());  
	}

	@Given("^I am in storeLocatorpage$")
	public void i_am_in_storeLocatorpage() throws Throwable {
		System.setProperty("webdriver.chrome.driver","C:/Users/sures/OneDrive/Desktop/Automation/chromedriver.exe");
        driver=new ChromeDriver();
		driver.get("https://www.lkbennett.com/");
	    driver.manage().window().maximize();    
	    Assert.assertEquals("L.K.Bennett | Luxury Women�s Clothing & Fashion", driver.getTitle());
	    Actions action=new Actions(driver);
		WebElement element=driver.findElements(By.cssSelector(".icon-store-locator.icon-2x.icon--primary")).get(0);
        action.moveToElement(element).perform(); 
        driver.findElement(By.cssSelector(".anchor--plain.jsstorepopover.storeFinderPopover")).click();
        Assert.assertEquals("Store Locator | L.K.Bennett", driver.getTitle());

	}

	@When("^I enter valid postcode$")
	public void i_enter_valid_postcode() throws Throwable {
		driver.findElement(By.cssSelector("#storelocator-query")).clear();
        driver.findElement(By.cssSelector("#storelocator-query")).sendKeys("da1 2xa");
        driver.findElements(By.cssSelector(".icon-Search.icon-2x")).get(4).click();
	}

	@Then("^I should see the store locator search results page$")
	public void i_should_see_the_store_locator_search_results_page() throws Throwable {
		Assert.assertEquals("Store Locator | L.K.Bennett", driver.getTitle());  
	}

	@When("^I enter invalid postcode$")
	public void i_enter_invalid_postcode() throws Throwable {
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		 driver.findElement(By.cssSelector("#storelocator-query")).clear();
	        driver.findElement(By.cssSelector("#storelocator-query")).sendKeys("da1");
	        driver.findElements(By.cssSelector(".icon-Search.icon-2x")).get(4).click(); 
	}

	@Then("^I should see the invalid store locator page$")
	public void i_should_see_the_invalid_store_locator_page() throws Throwable {
		Assert.assertEquals("Store Locator | L.K.Bennett", driver.getTitle());     
	}

	@When("^I leave the blank space and enter$")
	public void i_leave_the_blank_space_and_enter() throws Throwable {
		 driver.findElement(By.cssSelector("#storelocator-query")).clear();
	        driver.findElement(By.cssSelector("#storelocator-query")).sendKeys("  ");
	        driver.findElements(By.cssSelector(".icon-Search.icon-2x")).get(4).click();
	}

	@Then("^I should see the suggestion page$")
	public void i_should_see_the_suggestion_page() throws Throwable {
		 Assert.assertEquals("Store Locator | L.K.Bennett", driver.getTitle());
	}

	@When("^I enter special characters$")
	public void i_enter_special_characters() throws Throwable {
		 driver.findElement(By.cssSelector("#storelocator-query")).clear();
	        driver.findElement(By.cssSelector("#storelocator-query")).sendKeys("$%^&*");
	        driver.findElements(By.cssSelector(".icon-Search.icon-2x")).get(4).click();
	}

	@Then("^I should see therelated page$")
	public void i_should_see_therelated_page() throws Throwable {
		Assert.assertEquals("Store Locator | L.K.Bennett", driver.getTitle());
	}

	@When("^I click on findstore near me$")
	public void i_click_on_findstore_near_me() throws Throwable {
		driver.findElement(By.cssSelector("#findStoresNearMe")).click();
	}

	@Then("^I should see the result page$")
	public void i_should_see_the_result_page() throws Throwable {
		 Assert.assertEquals("Store Locator | L.K.Bennett", driver.getTitle());
	}
	@Given("^I am in home page$")
	public void i_am_in_home_page() throws Throwable {
		System.setProperty("webdriver.chrome.driver","C:/Users/sures/OneDrive/Desktop/Automation/chromedriver.exe");
        driver=new ChromeDriver();
		driver.get("https://www.lkbennett.com/");
	    driver.manage().window().maximize();  
	    
	}

	@When("^I click on signin$")
	public void i_click_on_signin() throws Throwable {
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	    Actions action=new Actions(driver);
		WebElement element=driver.findElements(By.cssSelector(".icon-wishlist.icon-2x")).get(0);
        action.moveToElement(element).perform();
        driver.findElements(By.cssSelector(".info-container .btn.btn--transparent")).get(0).click();
	}

	@Then("^I should be able to see signin page$")
	public void i_should_be_able_to_see_signin_page() throws Throwable {
		 Assert.assertEquals("https://www.lkbennett.com/login", driver.getCurrentUrl());
	}

	@When("^I click on register$")
	public void i_click_on_register() throws Throwable {
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		Actions action=new Actions(driver);
		WebElement element=driver.findElements(By.cssSelector(".icon-wishlist.icon-2x")).get(0);
        action.moveToElement(element).perform();
        driver.findElements(By.cssSelector(".info-container .btn")).get(1).click();
	}

	@Then("^I should be able to see the register page$")
	public void i_should_be_able_to_see_the_register_page() throws Throwable {
		Assert.assertEquals("https://www.lkbennett.com/login", driver.getCurrentUrl());
	}
	@When("^I click on facebook icon$")
	public void i_click_on_facebook_icon() throws Throwable {
		driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
		 JavascriptExecutor js = (JavascriptExecutor) driver;
		    js.executeScript("window.scrollBy(0,document.body.scrollHeight)");
		    driver.findElement(By.cssSelector(".continue-browse__link")).click();
	        driver.findElement(By.cssSelector(".icon-Facebook")).click(); 
	}

	@Then("^I should be redirected to the LKBennette facebook page$")
	public void i_should_be_redirected_to_the_LKBennette_facebook_page() throws Throwable {
		System.out.println("facebook page should be open");
		}

	@When("^I click on instagram icon$")
	public void i_click_on_instagram_icon() throws Throwable {
		driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
		JavascriptExecutor js = (JavascriptExecutor) driver;
	    js.executeScript("window.scrollBy(0,document.body.scrollHeight)");
	    driver.findElement(By.cssSelector(".continue-browse__link")).click();
	    driver.findElement(By.cssSelector(".icon-Instagram.icon--primary.icon-2x")).click();
	}

	@Then("^I should be able to see the instagram page$")
	public void i_should_be_able_to_see_the_instagram_page() throws Throwable {
	   System.out.println("instagram page opened");
	}

	@When("^I click on twitter icon$")
	public void i_click_on_twitter_icon() throws Throwable {
		driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
		JavascriptExecutor js = (JavascriptExecutor) driver;
	    js.executeScript("window.scrollBy(0,document.body.scrollHeight)");
	    driver.findElement(By.cssSelector(".continue-browse__link")).click();
	    driver.findElement(By.cssSelector(".icon-Twitter.icon--primary.icon-2x")).click();
	  
	}

	@Then("^I should be able to see the twitter page$")
	public void i_should_be_able_to_see_the_twitter_page() throws Throwable {
	  System.out.println("twitter page is opened");  
	}

	@When("^I click on youtube icon$")
	public void i_click_on_youtube_icon() throws Throwable {
		driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
		JavascriptExecutor js = (JavascriptExecutor) driver;
	    js.executeScript("window.scrollBy(0,document.body.scrollHeight)");
	    driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
	    driver.findElement(By.cssSelector(".continue-browse__link")).click();	   
	    driver.findElement(By.cssSelector(".icon-Youtube.icon--primary.icon-2x")).click();
	    
	}

	@Then("^I should be able to see the youtube page$")
	public void i_should_be_able_to_see_the_youtube_page() throws Throwable {
		System.out.println("youtube pageis opened");
	  	}

	@When("^I click on pinterest icon$")
	public void i_click_on_pinterest_icon() throws Throwable {
		driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
		JavascriptExecutor js = (JavascriptExecutor) driver;
	    js.executeScript("window.scrollBy(0,document.body.scrollHeight)");
	    driver.findElement(By.cssSelector(".continue-browse__link")).click();	  
	    driver.findElement(By.cssSelector(".icon-Pinterest.icon--primary.icon-2x")).click();  
	}

	@Then("^I should be able to see the pinterest page$")
	public void i_should_be_able_to_see_the_pinterest_page() throws Throwable {
	   System.out.println("pinterest page is opened");
	}



}	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	