@test
Feature: footer functionalities
Background:
Given I am in home page

Scenario: Functionality of social media facebook

When I click on facebook icon
Then I should be redirected to the LKBennette facebook page

Scenario: Functionality of social media instagram

When I click on instagram icon
Then I should be able to see the instagram page

Scenario: Functionality of social media twitter

When I click on twitter icon
Then I should be able to see the twitter page

Scenario: Functionality of social media youtube

When I click on youtube icon
Then I should be able to see the youtube page

Scenario: Functionality of social media pinterest

When I click on pinterest icon
Then I should be able to see the pinterest page
