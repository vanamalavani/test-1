Feature:  Search

Scenario: verify search with valid name

 Given I am in homepage.
 When I enter valid name
 Then I should be able to see the search results page

 Scenario: varify search with invalid name
 
 Given I am in homepage.
 When I enter invalid name
 Then I should be able to see the invalid search results page.



 Scenario: verify search with empty data
 
 Given I am in homepage.
 When I enter empty space.
 Then I should be able to see the search results page for empty data
 