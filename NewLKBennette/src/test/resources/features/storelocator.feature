Feature: Store Locator
Background:
Given I am in storeLocatorpage

Scenario: Store locator with valid postcode

When I enter valid postcode
Then I should see the store locator search results page

Scenario: Store locator with invalid postcode

When I enter invalid postcode
Then I should see the invalid store locator page

Scenario: Store locator with empty data

When I leave the blank space and enter
Then I should see the suggestion page

Scenario: Store locator with special characters

When I enter special characters
Then I should see therelated page

Scenario: Find store near me

When I click on findstore near me
Then I should see the result page