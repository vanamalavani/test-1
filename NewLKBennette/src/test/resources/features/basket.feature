Feature: Basket
Background:
 Given I am in searchresults page

Scenario: Basket with search

When I click add product
Then the product should be added to the basket

Scenario: Basket with mouse hovering

When I click add product
Then the mousehoverproduct should be added to the basket
 
Scenario: basket without selecting size

When I am clicking ad to basket without selecting size
Then selectsizeerror message should be displayed.

Scenario: Basket with out of stock item

When I enter out of stock item
Then I should see an error message with contact me button
@ignore
Scenario: Contact me when stock back button

When I click on the wish to contact me button
Then I should see the enter details page
 
Scenario: basket with maximum quantity

When I enter the maximum no of items
Then I should see the error message reached max

